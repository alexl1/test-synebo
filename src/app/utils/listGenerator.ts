import {ListItem} from '../models/ListItem';

const wordsSource = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dicta fuga mollitia nemo, neque nostrum\n' +
    'quibusdam quidem quos reprehenderit sed similique temporibus voluptatem voluptates. Accusantium eos inventore omnis\n' +
    'repudiandae tempore. Architecto beatae blanditiis consectetur cumque cupiditate doloribus eligendi est explicabo fugiat ' +
    'illum itaque necessitatibus odit officia quo quod quos, ratione recusandae sed similique tempore tenetur unde veniam. Dolorum\n' +
    'eaque, nihil? Aliquid dicta itaque laboriosam neque omnis? Adipisci beatae dolorem enim error excepturi, libero minima nemo odit\n' +
    'quia quod unde, velit voluptas voluptatibus. Accusantium, adipisci aperiam deleniti ea eum provident velit! Ab accusantium beatae' +
    'cupiditate dolor ducimus ea eligendi facilis in, ipsam neque nostrum quasi quidem quo\n' +
    'voluptate voluptates. Ad animi blanditiis corporis culpa dolores doloribus ipsum numquam optio, sit soluta! Aliquid beatae commodi ' +
    'cupiditate distinctio, expedita explicabo facere laboriosam minima pariatur qui, similique\n' +
    'suscipit. Aliquid at, commodi cumque eos iure magnam neque repellendus reprehenderit rerum suscipit! Commodi ea id\n' +
    'velit?'.split(' ');
const wordsSourceArr = wordsSource.split(' ');

const getRandStr = (minWords: number = 3, maxWords: number = 12) => {
    const strWordsCol = Math.floor(Math.random() * (maxWords - minWords) + minWords);
    let newStr = '';

    for ( let i = 0; i < strWordsCol; i++ ) {
        newStr += ` ${wordsSourceArr[Math.round(Math.random() * (wordsSourceArr.length - 1) + 1)]} `;
    }

    return newStr;
};

const generateList = (col: number = 100): ListItem[] => {
    return new Array(col).fill('').map( (item, index) => {
        return {
            id: Math.floor(Math.random() * (100000 - 1) + 1),
            position: index + 1,
            text: getRandStr()
        };
    });
};

export default generateList;
