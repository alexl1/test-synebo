import {Pipe, PipeTransform} from '@angular/core';

import {ListItem} from '../models/ListItem';

@Pipe({
    name: 'pagination'
})
export class PaginationPipe implements PipeTransform {
    transform(items: ListItem[], from: number, to: number): ListItem[] {
        return items.slice((from - 1) * to, to * from);
    }
}
