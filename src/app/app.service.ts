import {Injectable} from '@angular/core';

import {ITEMS_STORAGE_NAME} from './config';

import listGenerator from './utils/listGenerator';
import {ListItem} from './models/ListItem';

@Injectable()
export class AppService {
    getItems(): ListItem[] {
        if ( !localStorage.getItem(ITEMS_STORAGE_NAME) ) {
            localStorage.setItem(ITEMS_STORAGE_NAME, JSON.stringify(listGenerator()));
            return this.getItems();
        }

        return JSON.parse(localStorage.getItem(ITEMS_STORAGE_NAME));
    }

    listItemHandler(action: string, item: ListItem, oldVal?: string, callback?: () => void) {
        const allItems = this.getItems();
        let oneItem;
        let oneItemIndex;

        if ( oldVal ) {
            oneItem = allItems.find( (fItem: ListItem) => fItem.text === oldVal && fItem.position === item.position);
            oneItemIndex = allItems.findIndex( (fItem: ListItem) => fItem.text === oldVal && fItem.position === item.position);
        } else {
            oneItem = allItems.find( (fItem: ListItem) => fItem.text === item.text && fItem.position === item.position);
            oneItemIndex = allItems.findIndex( (fItem: ListItem) => fItem.text === item.text && fItem.position === item.position);
        }

        switch (action) {
            case 'edit':
                oneItem.text = item.text;
                localStorage.setItem(ITEMS_STORAGE_NAME, JSON.stringify(allItems));
                break;
            case 'remove':
                allItems.splice(oneItemIndex, 1);
                localStorage.setItem(ITEMS_STORAGE_NAME, JSON.stringify(allItems));
                callback();
                break;
            case 'add':
                allItems.push(item);
                localStorage.setItem(ITEMS_STORAGE_NAME, JSON.stringify(allItems));
                callback();
                break;
            default:
                console.error(`Action: "${action}" is not defined!`);
                alert(`Action: "${action}" is not defined!`);
        }
    }

    saveListOrder(list: ListItem[], callback?: () => void) {
        localStorage.setItem(ITEMS_STORAGE_NAME, JSON.stringify(list));
        callback();
    }
}
