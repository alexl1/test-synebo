export interface MenuItem {
    url: string;
    title: string;
}
