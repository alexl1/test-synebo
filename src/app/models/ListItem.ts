export interface ListItem {
    id: number;
    position: number;
    text: string;
}
