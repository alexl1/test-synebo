import {Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';

import {ListItem} from '../../models/ListItem';

import {AppService} from '../../app.service';

import {PaginationPipe} from '../../utils/pagination.pipe';

@Component({
    selector: 'app-screen-one',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
    list: ListItem[] = [];
    pagination = {
        limit: 10,
        currentPage: 1,
        total: null
    };
    newItemform = this.fb.group({
        text: ['', Validators.required]
    });

    constructor(
        public appService: AppService,
        private cdRef: ChangeDetectorRef,
        private fb: FormBuilder,
        private sb: MatSnackBar,
        private paginationPipe: PaginationPipe
    ) {}

    ngOnInit(): void {
        this.getItems();
    }

    private getItems() {
        this.list = this.appService.getItems().sort( (a, b) => a.position - b.position);

        this.paginationHandler();
    }

    paginationHandler(pageNum?: number) {
        if ( pageNum )
            return this.pagination.currentPage = pageNum;

        const paginationTotal = Math.ceil(this.list.length / this.pagination.limit);
        this.pagination.total = new Array(paginationTotal).fill('').map( (item, index) => index + 1);
    }

    listItemRemoved(e) {
        this.appService.listItemHandler('remove', e, null, () => {
            this.getItems();
            this.cdRef.detectChanges();
        });
    }

    addNewItem() {
        if ( this.newItemform.controls.text.hasError('required') ) {
            return this.sb.open('New item text is required!', 'ok', {
                duration: 2000
            });
        }
        let maxPosition = 0;
        this.list.forEach( item => {
            if ( item.position > maxPosition )
                maxPosition = item.position;
        });

        const newItem: ListItem = {
            id: Math.floor(Math.random() * (100000 - 1) + 1),
            text: this.newItemform.getRawValue().text,
            position: maxPosition + 1
        };
        this.appService.listItemHandler('add', newItem, null, () => {
            this.getItems();
            this.newItemform.reset();
        });
    }

    drop(e) {
        const currentItems = this.paginationPipe.transform(this.list, this.pagination.currentPage, this.pagination.limit);
        const dragedItem = currentItems.splice(e.previousIndex, 1)[0];
        let isIncrement = true;

        if ( !dragedItem ) {
            console.error('Dragged item is not defined');
            return false;
        }

        dragedItem.position = dragedItem.position - (e.previousIndex - e.currentIndex);

        this.list = this.list.map( item => {
            if ( item.position >= dragedItem.position && item.id !== dragedItem.id && isIncrement ) {
                 item.position++;
            }
            if ( item.id === dragedItem.id ) {
                isIncrement = false;
            }
            return item;
        });

        this.appService.saveListOrder(this.list, () => this.getItems());
    }
}
