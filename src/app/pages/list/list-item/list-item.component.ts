import {Component, Input, Output, EventEmitter, OnDestroy} from '@angular/core';
import {MatSnackBar} from '@angular/material';

import {ListItem} from '../../../models/ListItem';

import {AppService} from '../../../app.service';

@Component({
    selector: 'app-list-item',
    templateUrl: './list-item.component.html',
    styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnDestroy {
    @Input() cont: ListItem;
    @Input() innerClasses: string;
    @Output('removed') removed: EventEmitter<ListItem> = new EventEmitter();

    editMode = false;
    private oldVal: string;
    private subscribers = [];

    constructor(
        public appService: AppService,
        private sb: MatSnackBar
    ) {}

    ngOnDestroy(): void {
        this.subscribers.forEach( subsriber => subsriber.unsubscribe());
    }

    itemHandler(action: string) {
        switch (action) {
            case 'edit':
                if ( this.editMode ) return;

                this.editMode = true;
                this.oldVal = this.cont.text;
                break;
            case 'save':
                this.editMode = false;
                this.appService.listItemHandler('edit', this.cont, this.oldVal);
                break;
            case 'remove':
                const snackBar = this.sb.open('Do you really want to delete the item?', 'Yes', {
                    duration: 5000
                });
                this.subscribers.push(
                    snackBar.afterDismissed().subscribe( ev => {
                        if ( ev.dismissedByAction )
                            this.removed.emit(this.cont);
                    })
                );
                break;
            default:
                alert(`Action: "${action}" is not defined`);
                console.error(`Action: "${action}" is not defined`);
        }
    }
}
