import {Component} from '@angular/core';

@Component({
    selector: 'app-blank',
    template: '<p>Hi! I\'m blank component!</p>'
})
export class BlankComponent {}
