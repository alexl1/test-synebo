import {Component} from '@angular/core';

import {MenuItem} from '../../models/MenuItem';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
    menuList: MenuItem[] = [
        {
            url: 'list',
            title: 'list'
        },
        {
            url: 'blank',
            title: 'blank'
        }
    ];
}
