import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatButtonModule, MatIconModule, MatSnackBarModule, MatInputModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {DragDropModule} from '@angular/cdk/drag-drop';

import {AppRoutingModule} from './app-routing.module';
import {ComponentsModule} from './components/components.module';

import {AppComponent} from './app.component';
import {ListComponent} from './pages/list/list.component';
import {ListItemComponent} from './pages/list/list-item/list-item.component';
import {BlankComponent} from './pages/blank/blank.component';

import {AppService} from './app.service';

import {PaginationPipe} from './utils/pagination.pipe';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        ComponentsModule,
        MatButtonModule,
        MatIconModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        MatInputModule,
        ReactiveFormsModule,
        DragDropModule
    ],
    declarations: [
        AppComponent,
        ListComponent,
        BlankComponent,
        ListItemComponent,
        PaginationPipe
    ],
    providers: [
        AppService,
        PaginationPipe
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
